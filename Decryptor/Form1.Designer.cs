﻿namespace Decryptor
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblCle = new System.Windows.Forms.Label();
            this.lblDecrypt = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.txtCle = new System.Windows.Forms.TextBox();
            this.txtDecrypt = new System.Windows.Forms.TextBox();
            this.txtEncrypt = new System.Windows.Forms.TextBox();
            this.lblEncrypt = new System.Windows.Forms.Label();
            this.btnEncrypter = new System.Windows.Forms.Button();
            this.lblTitre = new System.Windows.Forms.Label();
            this.btnDecrypter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(30, 54);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(50, 13);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "Message";
            // 
            // lblCle
            // 
            this.lblCle.AutoSize = true;
            this.lblCle.Location = new System.Drawing.Point(30, 128);
            this.lblCle.Name = "lblCle";
            this.lblCle.Size = new System.Drawing.Size(22, 13);
            this.lblCle.TabIndex = 1;
            this.lblCle.Text = "Cle";
            // 
            // lblDecrypt
            // 
            this.lblDecrypt.AutoSize = true;
            this.lblDecrypt.Location = new System.Drawing.Point(30, 171);
            this.lblDecrypt.Name = "lblDecrypt";
            this.lblDecrypt.Size = new System.Drawing.Size(58, 13);
            this.lblDecrypt.TabIndex = 2;
            this.lblDecrypt.Text = "Decryption";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(98, 54);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(322, 55);
            this.txtMessage.TabIndex = 3;
            // 
            // txtCle
            // 
            this.txtCle.Location = new System.Drawing.Point(98, 128);
            this.txtCle.Name = "txtCle";
            this.txtCle.Size = new System.Drawing.Size(322, 20);
            this.txtCle.TabIndex = 4;
            // 
            // txtDecrypt
            // 
            this.txtDecrypt.Location = new System.Drawing.Point(98, 168);
            this.txtDecrypt.Multiline = true;
            this.txtDecrypt.Name = "txtDecrypt";
            this.txtDecrypt.Size = new System.Drawing.Size(322, 104);
            this.txtDecrypt.TabIndex = 5;
            // 
            // txtEncrypt
            // 
            this.txtEncrypt.Location = new System.Drawing.Point(98, 278);
            this.txtEncrypt.Multiline = true;
            this.txtEncrypt.Name = "txtEncrypt";
            this.txtEncrypt.Size = new System.Drawing.Size(322, 104);
            this.txtEncrypt.TabIndex = 7;
            // 
            // lblEncrypt
            // 
            this.lblEncrypt.AutoSize = true;
            this.lblEncrypt.Location = new System.Drawing.Point(30, 281);
            this.lblEncrypt.Name = "lblEncrypt";
            this.lblEncrypt.Size = new System.Drawing.Size(57, 13);
            this.lblEncrypt.TabIndex = 6;
            this.lblEncrypt.Text = "Encryption";
            // 
            // btnEncrypter
            // 
            this.btnEncrypter.Location = new System.Drawing.Point(33, 406);
            this.btnEncrypter.Name = "btnEncrypter";
            this.btnEncrypter.Size = new System.Drawing.Size(167, 71);
            this.btnEncrypter.TabIndex = 8;
            this.btnEncrypter.Text = "Encrypter";
            this.btnEncrypter.UseVisualStyleBackColor = true;
            this.btnEncrypter.Click += new System.EventHandler(this.btnTraduire_Click);
            // 
            // lblTitre
            // 
            this.lblTitre.AutoSize = true;
            this.lblTitre.Location = new System.Drawing.Point(30, 23);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(151, 13);
            this.lblTitre.TabIndex = 9;
            this.lblTitre.Text = "Code secret entre Emile et will!";
            // 
            // btnDecrypter
            // 
            this.btnDecrypter.Location = new System.Drawing.Point(233, 406);
            this.btnDecrypter.Name = "btnDecrypter";
            this.btnDecrypter.Size = new System.Drawing.Size(167, 71);
            this.btnDecrypter.TabIndex = 10;
            this.btnDecrypter.Text = "Decrypter";
            this.btnDecrypter.UseVisualStyleBackColor = true;
            this.btnDecrypter.Click += new System.EventHandler(this.btnDecrypter_Click_1);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(39, 501);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(360, 68);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Decryptor.Properties.Resources.emile;
            this.ClientSize = new System.Drawing.Size(440, 588);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnDecrypter);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.btnEncrypter);
            this.Controls.Add(this.txtEncrypt);
            this.Controls.Add(this.lblEncrypt);
            this.Controls.Add(this.txtDecrypt);
            this.Controls.Add(this.txtCle);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.lblDecrypt);
            this.Controls.Add(this.lblCle);
            this.Controls.Add(this.lblMessage);
            this.Name = "frmPrincipal";
            this.Text = "Messages secrets";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblCle;
        private System.Windows.Forms.Label lblDecrypt;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.TextBox txtCle;
        private System.Windows.Forms.TextBox txtDecrypt;
        private System.Windows.Forms.TextBox txtEncrypt;
        private System.Windows.Forms.Label lblEncrypt;
        private System.Windows.Forms.Button btnEncrypter;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.Button btnDecrypter;
        private System.Windows.Forms.Button btnClear;
    }
}

