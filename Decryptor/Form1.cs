﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decryptor
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void btnTraduire_Click(object sender, EventArgs e)
        {
            Cryptographie crypt = new Cryptographie(txtCle.Text, txtMessage.Text);
            //encrypt
            txtEncrypt.Text = crypt.Encrypter();
            txtDecrypt.Clear();
            txtEncrypt.ReadOnly = false;
            txtDecrypt.ReadOnly = true;



        }

        

        private void btnDecrypter_Click_1(object sender, EventArgs e)
        {
            Cryptographie crypt = new Cryptographie(txtCle.Text, txtMessage.Text);
            //decrypt
            txtDecrypt.Text = crypt.Decrypter();
            txtEncrypt.Clear();
            txtEncrypt.ReadOnly = true;
            txtDecrypt.ReadOnly = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtDecrypt.Clear();
            txtEncrypt.Clear();
            txtMessage.Clear();
            txtCle.Clear();
        }
    }
}
