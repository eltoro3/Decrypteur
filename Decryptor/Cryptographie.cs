﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decryptor
{
    class Cryptographie
    {
        //attributs
        private string cle;
        private string messageSecret;

        //accesseurs
        public string MessageSecret
        {
            get { return messageSecret; }
            set { messageSecret = value; }
        }

        public string Cle
        {
            get { return cle; }
            set { cle = value; }
        }
        //constructeur
        public Cryptographie(string cle, string message)
        {
            this.Cle = cle;
            this.MessageSecret = message;
        }
        //methodes
        public string Encrypter()
        {
            int longueurMessage = this.MessageSecret.Length;
            int valeurASCIIMessage;

            int longueurCle = this.Cle.Length;
            int cleCompteur = 0;
            int valeurASCIICle;
            string messageEncrypte = string.Empty;

            for (int i = 0; i <= longueurMessage - 1; i++)
            {

                valeurASCIIMessage = this.MessageSecret[i];
                valeurASCIICle = this.Cle[cleCompteur];


                //manipulation
                int additionCleMessage = valeurASCIICle + valeurASCIIMessage;
                byte binaireInvAddition = (byte)~additionCleMessage;
                string hex = binaireInvAddition.ToString("X");
                messageEncrypte += hex;
                //incrementation du compteur pour le caractere de la cle

                cleCompteur++;
                if (cleCompteur == longueurCle)
                {
                    cleCompteur = 0;
                }

            }
            return messageEncrypte ;
        }
        public string Decrypter()
        {
            string messageDecrypte = string.Empty;
            int longueurMessage = MessageSecret.Length;
            int cleCompteur = 0;

            for (int i = 0; i < longueurMessage; i += 2)
            {
                //on separe chaque couple pour les convertir en binaire
                string hexCarac = MessageSecret[i] + "" + MessageSecret[i + 1];
                int longueurCle = this.Cle.Length;

                int valeurIntDeHex = Convert.ToInt32(hexCarac, 16);
                byte inverse = (byte)~valeurIntDeHex;

                if (cleCompteur == longueurCle)
                {
                    cleCompteur = 0;
                }

                int inverseIntMoinsCle = Convert.ToInt32(inverse) - Convert.ToInt16(cle[cleCompteur]);
                char caractere = (char)inverseIntMoinsCle;
                messageDecrypte += Convert.ToString(caractere);
                //Console.WriteLine(hexCarac +"  "+valeurIntDeHex +"  "+cleCompteur+"  "+inverseIntMoinsCle + "  " +caractere);
                cleCompteur++;

            }
            return  messageDecrypte;
        }
    }
}
