﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageSecret
{
    class Encryption
    {
        static void Main(string[] args)
        {
            bool program = true;
            while (program) {
                bool check = false;
                string choix = string.Empty;
                while (!check) {
                    Console.WriteLine("Veuillez choisir une option " +
                        "\nE = Encrypter un message" +
                        "\nD = Decrypter un message" +
                        "\nQ = Quitter le programme");
                    choix = Console.ReadLine();
                    if (choix == "E" || choix == "e" || choix == "D" || choix == "d"||choix == "Q" ||choix == "q")
                    {
                        if (choix == "E" || choix == "e")
                        {
                            choix = "1";
                        }
                        else if(choix == "D" || choix == "d")
                        {
                            choix = "2";
                        }
                        else
                        {
                            program = false;
                        }
                        check = true;

                    }
                    else
                    {
                        Console.WriteLine("Entrez une option valide");
                    }
                }
                if (choix == "1")//encrypter une message
                {
                    Console.WriteLine("Entrez un message secret");
                    string message = Console.ReadLine();
                    Console.WriteLine("Entrez une cle d'encryption");
                    string cle = Console.ReadLine();
                    Cryptographie crypto = new Cryptographie(cle, message);
                    string encrypt = crypto.Encrypter();
                    Console.WriteLine(encrypt);

                }
                else if(choix == "2")
                {
                    Console.WriteLine("Entrez le mot encrypte");
                    string message = Console.ReadLine();
                    Console.WriteLine("Entrez la cle d'encryption");
                    string cle = Console.ReadLine();
                    Cryptographie crypto = new Cryptographie(cle, message);
                    string decrypt = crypto.Decrypter();
                    Console.WriteLine(decrypt);
                    //string decrypt = crypto.Decrypter(encrypt,cle);
                    //Console.WriteLine(decrypt);
                }


            }
            Console.WriteLine("Ciao!");
            Console.ReadKey();
        }
    }
}
