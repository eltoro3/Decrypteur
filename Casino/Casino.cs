﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Casino
    {
        static void Main(string[] args)
        {
            //Dialogue
            string menu = "Que voulez-vous faire?\n" +
                "1.Tirer une nouvelle carte\n" +
                "2.Poursuivre (check)\n";

            //Dialogue fin
            
            Joueur croupier = new Joueur("Croupier", 0, 0, 0, 0, 1000000000);

            Console.WriteLine("Entrez le nom de votre joueur: ");
            Joueur joueur = new Joueur(Console.ReadLine(), 0, 0, 0, 0, 50);


            
            


            //Partie commence 
            
            while (joueur.MontantArgent > 0)//tant que le joueur a de l'argent
            {
                Console.WriteLine("Vous avez gagne {0} parties sur {1} parties jouee et vous avez {2:c}\n" +
                        "---------------------------------------------------------", joueur.NbPartiesGagnees, joueur.NbPartiesJouees,joueur.MontantArgent);


                uint mise = 0;
                bool check = false;
                //On demande la mise du joueur
                while (!check)//Mise
                {
                    Console.WriteLine("Combien misez-vous?");
                    bool miseTry = uint.TryParse(Console.ReadLine(), out mise);
                    if (miseTry)
                    {

                        if (mise > joueur.MontantArgent)//Validation de son solde 
                        {
                            //la mise est valide
                            Console.WriteLine("Vous n'avez pas assez d'argent");

                        }
                        else
                        {
                            check = true;
                        }
                    }
                    else
                    {

                        Console.WriteLine("Entrez un nombre plus grand que zero)");
                    }
                    
                }
                //les joueurs pigent

                for (int i = 1; i<=2;i++)
                {
                    int carte = joueur.AjouterCarte();
                    Console.WriteLine("Vous avez piges le {0} de {1}",joueur.getCarteInfo(carte)[0], joueur.getCarteInfo(carte)[1]);

                    //le croupier pige
                    croupier.AjouterCarte();
                }
                Console.WriteLine("La somme de vos cartes est de: {0}",joueur.calculerSommeCarte());

                bool partieEnCour = true;

                while (partieEnCour)
                {
                    bool choix = false;
                    if (joueur.verifGagnant() || croupier.verifGagnant())
                    {
                        partieEnCour = false;
                        choix = true;
                        //le menu ne s'affichera pas!
                    }
                    
                    while (!choix)
                    {
                        Console.WriteLine(menu);
                        string optionChoisie = Console.ReadLine();
                        if (optionChoisie == "1")
                        {
                            int carte = joueur.AjouterCarte();
                            Console.WriteLine("Vous avez piges le {0} de {1}", joueur.getCarteInfo(carte)[0], joueur.getCarteInfo(carte)[1]);
                            
                            if (croupier.calculerSommeCarte() < 16)
                            {
                                croupier.AjouterCarte();
                                Console.WriteLine("Le croupier pige aussi....");
                            }
                            else
                            {
                                Console.WriteLine("Le croupier passe!");
                            }
                            Console.WriteLine("La somme de vos cartes est de: {0}", joueur.calculerSommeCarte());
                            if (joueur.verifGagnant() || croupier.verifGagnant())
                            {
                                partieEnCour = false;
                            }
                            choix = true;

                        } else if (optionChoisie == "2")
                        {
                            Console.WriteLine("Vous passez votre tour!");

                            while (croupier.calculerSommeCarte() < 16)
                            {
                                croupier.AjouterCarte();
                                Console.WriteLine("Le croupier pige une carte.....");
                            }
                            partieEnCour = false;
                            choix = true;
                        }
                        else
                        {
                            Console.WriteLine("Option invalide, vous devez ecrire soit 1 ou 2!");
                        }
                    }
                    
                    
                }
                //verif du gagnant
                int sommeCroupier = croupier.calculerSommeCarte(), sommeJoueur = joueur.calculerSommeCarte();
                bool joueurGagne;
                bool croupierGagne;
                string message = string.Empty;
                //cas ou le joueur est perdant-------------------------------------
                if (sommeCroupier == 21 || sommeJoueur > 21 || 21-sommeJoueur > 21-sommeCroupier)
                {
                    message = "Vous avez perdu! : ";
                    if (sommeCroupier == 21)
                    {
                        message += "Le croupier a 21 pile";
                    }else if (sommeJoueur > 21)
                    {
                        message += "Vous avez depasse 21";
                    }
                    else if(21 - sommeJoueur > 21 - sommeCroupier)
                    {
                        message += "Le croupier(" + sommeCroupier + ") est plus pres de 21 que vous(" + sommeJoueur + ")";
                    }
                    
                    joueurGagne = false;
                    croupierGagne = true;
                }
                else
                {
                    message = "Vous avez gagne! : ";
                    if (sommeJoueur == 21)
                    {
                        message += "Vous avez 21 pile";
                    }
                    else if (sommeCroupier > 21)
                    {
                        message += "Le croupier a depasse 21";
                    }
                    else if (21 - sommeJoueur < 21 - sommeCroupier)
                    {
                        message += "Vous(" + sommeJoueur + ") etes plus pres de 21 que le croupier(" + sommeCroupier+")";
                    }
                    
                    joueurGagne = true;
                    croupierGagne = false;
                }

                Console.WriteLine(message);
                //a la fin de la partie il faut reinitaliser les donnes de la partie
                joueur.terminerPartie(joueurGagne,mise);
                croupier.terminerPartie(croupierGagne, mise);
            }
            Console.WriteLine("Oh non! Vous vous etes ruine! Au revoir!");
            Console.ReadKey();
        }
    }

    
}
