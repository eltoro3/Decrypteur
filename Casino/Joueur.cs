﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Joueur
    {
        private string nom;
        private int totalValeursCartes;
        private int nbCartes;
        private int nbPartiesJouees;
        private int nbPartiesGagnees;
        private float montantArgent;
        private Random rnd = new Random();
        List<int> cartesPigees = new List<int>();




        public float MontantArgent
        {
            get { return montantArgent; }
            set
            {

                if (montantArgent < 0f)
                {
                    montantArgent = 0;
                }
                else
                {
                    montantArgent = value;
                }


            }
        }


        public int NbPartiesGagnees
        {
            get { return nbPartiesGagnees; }
            set
            {
                if (nbPartiesGagnees < 0)
                {
                    nbPartiesGagnees = 0;
                }
                else
                {
                    nbPartiesGagnees = value;
                }
            }
        }


        public int NbPartiesJouees
        {
            get { return nbPartiesJouees; }
            set
            {
                if (nbPartiesJouees < 0)
                {
                    nbPartiesJouees = 0;
                }
                else
                {
                    nbPartiesJouees = value;
                }
            }
        }


        public int NbCartes
        {
            get { return nbCartes; }
            set
            {
                if (nbCartes <= 0)
                {
                    nbCartes = 0;
                }
                else
                {
                    nbCartes = value;
                }
            }
        }

        public int TotalValeursCartes
        {
            get { return totalValeursCartes; }
            set
            {
                if (totalValeursCartes <= 0)
                {
                    totalValeursCartes = 0;
                }
                else
                {
                    totalValeursCartes = value;
                }
            }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        //constructeur 

        public Joueur(string nom, int tvc, int nc, int pj, int pg, float argent)
        {
            Nom = nom;
            TotalValeursCartes = tvc;
            NbCartes = nc;
            NbPartiesJouees = pj;
            NbPartiesGagnees = pg;
            MontantArgent = argent;
            

    }
        //fonctions
        /// <summary>
        /// fait piger une carte au joueur
        /// </summary>
        public int AjouterCarte()
        {


            int pige;
            pige = rnd.Next(1, 53);
            cartesPigees.Add(pige);
            NbCartes++;
            return pige;
        }


        /// <summary>
        /// Retourne les infos d'une carte selon son Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 

        public string[] getCarteInfo(int id)
        {
            string[] deck_sign = new string[4];

            deck_sign[0] = "Coeur";
            deck_sign[1] = "Trefle";
            deck_sign[2] = "Pique";
            deck_sign[3] = "Carreaux";


            string[] carte = new string[3];

            int cards = id;
            int valeur = (cards % 13);

            if (valeur == 0)
            {
                valeur = 13;
            }
            int signe = cards / 13;
            if (signe == 4)//si la carte 52 est pige signe = 3
            {
                signe = 3;
            }
            switch (valeur) {
                case 1: //as
                    carte[0] = "A";
                    break;
                case 11://valet
                    carte[0] = "J";
                    break;
                case 12://valet
                    carte[0] = "Q";
                    break;
                case 13://valet
                    carte[0] = "K";
                    break;
                default:
                    carte[0] = Convert.ToString(valeur);//valeur de la carte
                    break;


            }

           
            carte[1] = deck_sign[signe];//signe de la carte
            carte[2] = Convert.ToString(cards); //id de la carte

            return carte;


        }


        /// <summary>
        /// Gain et perte font en sorte que le joueur perde ou gagne de l'argent
        /// </summary>
        /// <param name="gain"></param>
        public void gain(uint gain)
        {
            MontantArgent += gain;
        }
        public void perte(uint perte)
        {
            MontantArgent -= perte;
        }
        /// <summary>
        /// Calcule la valeur de la somme des cartes pigees par un joueur
        /// </summary>
        /// <returns>La valeur des cartes (INT)</returns>
        public int calculerSommeCarte()
        {
            int somme = 0;
            int asPigees = 0;
            bool asPresent = false;//sil y a un as, il est pris en compte apres la boucle
            foreach (int c in this.cartesPigees)
            {
                if (this.getCarteInfo(c)[0] == "J" || this.getCarteInfo(c)[0] == "Q" || this.getCarteInfo(c)[0] == "K" )
                {
                    somme += 10;
                }else if (this.getCarteInfo(c)[0] == "A" )
                {
                     asPresent = true;
                    asPigees++;
                    
                }
                else//si carte n'est pas un as ou K,Q,J
                {
                    somme += int.Parse(this.getCarteInfo(c)[0]);
                }
            }

            if (asPresent)//traiteent as
            {
                for (int i =1; i<= asPigees; i++) {
                    if (somme + 11 > 21)//as vaut 1
                    {
                        somme++;
                    }
                    else//as vaut 11
                    {
                        somme += 11;
                    }
                }
            }
            
            return somme;
        }
        /// <summary>
        /// Reinitialise les valeurs du joueur 
        /// </summary>
        /// <param name="joueurGagnant">True, si le joueur a gagne</param>
        /// <param name="mise">La somme d'argent qui etatit misee</param>
        public void terminerPartie(bool joueurGagnant, uint mise) {
            if (joueurGagnant)
            {
                NbPartiesGagnees++;
                NbPartiesJouees++;
                this.gain(mise);
                cartesPigees.Clear();
                this.NbCartes = 0;
            }
            else
            {

                NbPartiesJouees++;
                this.perte(mise);
                cartesPigees.Clear();
                this.NbCartes = 0;
            }
        }

        /// <summary>
        /// Verfie sil y a un gagnant parce que les cartes on une somme de 21 ou plus
        /// </summary>
        /// <param name="j1"></param>
        /// <returns>True sil ya gagnant ou false sinon</returns>
        public bool verifGagnant()
        {
            int sommeJoueur = this.calculerSommeCarte();
            if (sommeJoueur == 21 || sommeJoueur > 21 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
