﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TES
{
    class Personnage
    {
        private string classe;
        private int pointsDeVie;
        private int pointsAttaque;
        private int pointsDefense;
        private int pointsDeVieMax;
        private string nom;
        private bool vivant = true;
        private string classeNom;
        public Random rnd;

        public int PointsDeVieMax
        {
            get { return pointsDeVieMax; }
            set
            {
                if (pointsDeVieMax < 0)
                {
                    pointsDeVieMax = 0;
                }
                else { pointsDeVieMax = value; }
            }
        }

        public string ClasseNom
        {
            get { return classeNom; }
            set { classeNom = value; }
        }
        public bool Vivant
        {
            get { return vivant; }
            set { vivant = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        

        public int PointsDefense
        {
            get { return pointsDefense; }
            set { if (pointsDefense < 0) {
                    pointsDefense = 0;
                } else { pointsDefense = value; } }
            }


        public int PointsAttaque
        {
            get { return pointsAttaque; }
            set
            {
                if (pointsAttaque < 0)
                {
                    pointsAttaque = 0;
                }
                else { pointsAttaque = value; }
            }
        }
    

        public int PointsDeVie
        {
            get { return pointsDeVie; }
            set
            {
                if (pointsDeVie < 0)
                {
                    pointsDeVie = 0;
                }
                else { pointsDeVie = value; }
            }
        }
    
    


        public string Classe
        {
            get { return classe; }
            set { classe = value; }
        }
        //constructeur
        public Personnage(string nom, string classe)
        {
            rnd = new Random();
            Nom = nom;
            int pvRnd = 0, paRnd = 0, pdRnd = 0;
            switch (classe){
                case "1"://archer
                     pvRnd = rnd.Next(1,11);
                     paRnd = rnd.Next(1,7);
                     pdRnd = rnd.Next(1,7);
                    ClasseNom = "Archer";
                    PointsDeVieMax = pvRnd;


                    break;
                case "2"://Mage
                    pvRnd = rnd.Next(1, 5);
                    paRnd = rnd.Next(1, 9);
                    pdRnd = rnd.Next(1, 7);
                    ClasseNom = "Mage";
                    PointsDeVieMax = pvRnd;

                    break;
                case "3"://guerrier
                    pvRnd = rnd.Next(2, 21);
                    paRnd = rnd.Next(1,11);
                    pdRnd = rnd.Next(1, 11);
                    ClasseNom = "Guerrier";
                    PointsDeVieMax = pvRnd;
                    break;
                case "4"://assassin
                    pvRnd = rnd.Next(1, 7);
                    paRnd = rnd.Next(1, 7);
                    pdRnd = rnd.Next(1, 8);
                    ClasseNom = "Assassin";
                    PointsDeVieMax = pvRnd;
                    break;
                case "5"://Moine
                    pvRnd = rnd.Next(1, 9);
                    paRnd = rnd.Next(1, 9);
                    pdRnd = rnd.Next(1, 9);
                    ClasseNom = "Moine";
                    PointsDeVieMax = pvRnd;
                    break;

            }
            Classe = classe;
            PointsDeVie = pvRnd;
            PointsAttaque = paRnd;
            PointsDefense = pdRnd;
            
        }
        //methodes
        public string Attaquer(Personnage ennemi)
        {
            string retour = string.Empty;

            int nbAleatoireAttaque = rnd.Next(1,9);
            int coupAttaque = this.PointsAttaque + nbAleatoireAttaque;

            int nbAleatoireDefense = rnd.Next(1, 11);
            int defEnnemi = ennemi.PointsDefense + nbAleatoireDefense;

            if (coupAttaque > defEnnemi) {
                int degat = rnd.Next(1,5);
                ennemi.PointsDeVie -= degat;
                retour = String.Format("{2} : Attaque avec succes! -{0} Points de vies afflige sur : {1}",degat,ennemi.Nom,this.Nom);
            }
            else
            {
                retour = String.Format("{0} : Attaque manque!",this.Nom);
            }

            //verif que les persos sont vivant
            if (ennemi.PointsDeVie < 0 )//ennemi mort
            {
                ennemi.Vivant = false;
            }
            return retour;
        }
        /// <summary>
        /// Le personnage se repose alors il gagne 2 PV
        /// </summary>
        /// <returns>Un log de son action en plus de son nouveau statut de PV</returns>
        public string Reposer()
        {
            string retour = string.Empty; 
            if (this.PointsDeVieMax == this.PointsDeVie) {
                retour = "Vous avez deja tous vos points de vie!";
            }
            else
            {
                if ((this.PointsDeVie + 2) > this.PointsDeVieMax)
                {
                    this.PointsDeVie = this.PointsDeVieMax;

                }
                else
                {
                    this.PointsDeVie += 2;
                    retour = string.Format("Vous avez gagne +2 Points de Vie, Vous avez maintenant {0}", this.PointsDeVie);
                }
                

            }
            return retour;
        }
        /// <summary>
        /// Fourni les stats sur un personnage
        /// </summary>
        /// <returns>Info dun personnage</returns>
        public string InfoPerosnnage()
        {
            string retour = string.Format("------------------------------------------\n" +
                "{0} vient d'etre genere! Voici ses stats : \n" +
                "{1,-3} Points de vie\n" +
                "{2,-3} Points d'attaque\n" +
                "{3,-3} Points de defense\n"+
                "------------------------------------------", this.Nom,this.PointsDeVie,this.PointsAttaque,this.PointsDefense);
            return retour;
        }
       
        


    }
}

