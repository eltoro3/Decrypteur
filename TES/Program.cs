﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TES
{
    class Program
    {
        static void Main(string[] args)
        {
            Personnage ennemi;
            Random rnd = new Random();
            string choix = string.Empty;
            string info = string.Empty;
            //on demande a lutilisateur de choisir ses parametres
            string type = "",nom = "";
            bool check = false;
            while (!check)
            {
                Console.WriteLine("Quel est la classe de votre premier hero?" +
                    "\n1=Archer" +
                    "\n2=Mage" +
                    "\n3=Guerrier" +
                    "\n4=Assassin" +
                    "\n5=Moine");
                type = Console.ReadLine();
                if (type == "1" || type == "2"||type == "3"||type == "4"||type == "5")
                {
                    check = true;
                }
                else
                {
                    Console.WriteLine("Option invalide!");
                }
            }
            Console.WriteLine("Quel est le nom de votre premier hero?");
            nom = Console.ReadLine();

            Personnage hero1 = new Personnage(nom,type);
            info = hero1.InfoPerosnnage();
            Console.WriteLine(info);
            check = false;
            while (!check)
            {
                Console.WriteLine("Quel est la classe de votre deuxieme hero?" +
                    "\n1=Archer" +
                    "\n2=Mage" +
                    "\n3=Guerrier" +
                    "\n4=Assassin" +
                    "\n5=Moine");
                type = Console.ReadLine();
                if (type == "1" || type == "2" || type == "3" || type == "4" || type == "5")
                {
                    check = true;
                }
                else
                {
                    Console.WriteLine("Option invalide!");
                }
            }
            Console.WriteLine("Quel est le nom de votre deuxieme hero?");
            nom = Console.ReadLine();

            Personnage hero2 = new Personnage(nom, type);
            info = hero2.InfoPerosnnage();
            Console.WriteLine(info);
            bool partieEnCours = true;
            while (partieEnCours)
            {
                //Choix du joueur
                bool montrerChoixJoueur = true;
                while (montrerChoixJoueur)
                {
                    Console.WriteLine("Quel hero voulez-vous utiliser?" +
                        "\n 1.{0} ({2,5})" +
                        "\n 2.{1} ({3,5})",hero1.Nom,hero2.Nom,hero1.ClasseNom,hero2.ClasseNom);

                    bool choixJoueur = false;
                    while (!choixJoueur)
                    {
                        choix = Console.ReadLine();
                        if (choix == "1" || choix == "2" )
                        {
                            choixJoueur = true;
                            montrerChoixJoueur = false;
                        }
                        else
                        {
                            Console.WriteLine("Choix invalide");
                        }
                    }
                }

                //Actions du joueur 
                bool montrerMenuJoueur = true;
                while (montrerMenuJoueur)
                {
                    Console.WriteLine("Que voulez faire?" +
                    "\n1: Attaquer un ennemi" +
                    "\n2: Reposer");
                    string option = Console.ReadLine();
                    if (option == "1" || option =="2")
                    {
                        montrerMenuJoueur = false;
                        if (option == "1")//il chosi 1
                        {
                            //on attaque
                            string classeEnnemi = Convert.ToString(rnd.Next(1,6));

                            ennemi = new Personnage("Le Horla!",classeEnnemi);
                            if (choix == "1") {
                                while (hero1.Vivant && ennemi.Vivant)//le hero 1 se bat
                                {
                                    //le hero attaque
                                    string attaqueHero = hero1.Attaquer(ennemi);
                                    Console.WriteLine(attaqueHero);
                                    //le monstre attaque
                                    string attaqueEnnemi = ennemi.Attaquer(hero1);
                                    Console.WriteLine(attaqueEnnemi);
                                }
                                //verif du gagnant du combat
                                if (!hero1.Vivant)
                                {
                                    Console.WriteLine("Oh non! Votre hero {0} est mort", hero1.Nom);
                                }
                                else
                                {
                                    Console.WriteLine("Votre hero {0} a vaincu son ennemi et il lui reste {1} Points de vie", hero1.Nom, hero1.PointsDeVie);
                                }
                            }
                            else
                            {
                                while (hero2.Vivant && ennemi.Vivant)//le hero 2 se bat
                                {
                                    //le hero attaque
                                    string attaqueHero = hero2.Attaquer(ennemi);
                                    Console.WriteLine(attaqueHero);
                                    //le monstre attaque
                                    string attaqueEnnemi = ennemi.Attaquer(hero2);
                                    Console.WriteLine(attaqueEnnemi);
                                }
                                //verif du gagnant du combat
                                if (hero2.PointsDeVie > 0)
                                {
                                    Console.WriteLine("Oh non! Votre hero {0} est mort",hero2.Nom);
                                }
                                else
                                {
                                    Console.WriteLine("Votre hero {0} a {1} Points de vie", hero2.Nom,hero2.PointsDeVie);
                                }
                                
                            }

                        }
                        else {//il choisi 2
                            string repos = string.Empty;
                            if (choix == "1")//hero 1
                            {
                                repos = hero1.Reposer();
                            }else {//hero 2
                                repos = hero2.Reposer();
                            }
                            Console.WriteLine(repos);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Option invalide");
                    }
                }
                

            }

            
           
        }
    }
}
