﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cardio
{
    class Client
    {
        //attributes
        private string nom;
        private int age;
        private string typeAbonnement;
        private decimal coutActivite;
        private int dureAbonnement;
        private float rabaisActivite;
        private const float rabais60Ans = 0.3f;
        
        public float RabaisActivite
        {
            get { return rabaisActivite; }
            set
            {
                if (rabaisActivite < 0)
                {
                    rabaisActivite = 0;
                }
                else
                {
                    rabaisActivite = value;
                }
            }
        }


        //properties
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public int Age
        {
            get { return age; }
            set
            {
                if (age <= 5 && age < 120)//Nous jugeons que 120 ans est la limite d'age
                {
                    age = 0;
                }
                else
                {
                    age = value;
                }
            }
        }

        public string TypeAbonnement
        {
            get { return typeAbonnement; }
            set { typeAbonnement = value; }
        }
        public decimal CoutActivite
        {
            get { return coutActivite; }
            set {
                if (coutActivite < 0)
                {
                    coutActivite = 0;
                }
                else { coutActivite = value; }
            }

        }
        public int DureAbonnement
        {
            get { return dureAbonnement; }
            set {
                if (dureAbonnement < 0) {
                    dureAbonnement = 0;
                }
                else { dureAbonnement = value; }
            }
        }


        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="nom">Nom du client</param>
        /// <param name="a">Age</param>
        /// <param name="abo">Type d'abonnement</param>
        /// <param name="act">Cout des activites choisies</param>
        /// <param name="dure">Dure de l'abo</param>
        public Client(string nom,int age, string type,float coutAct,int dure,float rabaisAct)
        {
            Nom = nom;
            Age = age;
            TypeAbonnement = type;//1,2,3
            CoutActivite = Convert.ToDecimal(coutAct);
            DureAbonnement = dure;
            RabaisActivite = rabaisAct;
            
            


          
        }
        public string genererFacture()
        {
            int coutAnnuel = 0;
            string rabaisApplicable60Ans;
            //cout annuel
            switch (this.TypeAbonnement) {
                case "1":
                    coutAnnuel = 100;
                    break;
                case "2":
                    coutAnnuel = 150;
                    break;
                case "3":
                    coutAnnuel = 200;
                    break;
            }
            //cout activites
            decimal coutAcTotal = this.CoutActivite - (this.CoutActivite * Convert.ToDecimal(this.RabaisActivite));
            decimal sousTotal = coutAnnuel + coutAcTotal;
            decimal sousTotalApresRabais60;
            if (this.Age >= 60)
            {
                sousTotalApresRabais60 = sousTotal + (sousTotal * Convert.ToDecimal(rabais60Ans));
                rabaisApplicable60Ans = "30%";
            }
            else
            {
                rabaisApplicable60Ans = "0%";
                sousTotalApresRabais60 = sousTotal;
            }

            decimal coutTPS = 0.05M * sousTotalApresRabais60, coutTVQ = 0.0975M * sousTotalApresRabais60;
            decimal total = sousTotalApresRabais60 + coutTVQ + coutTPS;
            string facture = string.Empty;





            facture = string.Format("" +
                "Cout annuel                  : {0,5:c}\n" +
                "Cout des activites           : {1,5:c}\n" +
                "Cout des activites(- {2}%)    : {3,5:c}\n" +
                "Sous-total                   : {4,5:c}\n" +
                "Sous-total (- rabais 60+)    : {5,5:c}\n" +
                "TPS (5%)                     : {6,5:c}\n" +
                "TVQ (9.975%)                 : {7,5:c}\n" +
                "Total                        : {8,5:c}", coutAnnuel,this.CoutActivite,this.RabaisActivite *100,coutAcTotal,sousTotal,sousTotalApresRabais60,coutTPS,coutTVQ,total);
                return facture;
        }

    }
}
