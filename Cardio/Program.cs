﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cardio
{
    class Program
    {
        static void Main(string[] args)
        {
            string type = string.Empty;
            bool check = false;
            
            bool rabaisPlusDe60 = false;
            float rabais;
            
            float coutActivite = 0;
            int dure = 0;
            float rabaisActivite = 0;


            //nom
            Console.WriteLine("Quelle est votre nom?");
            string nom = Console.ReadLine();
            //age
            int age = 0;
            check = false;
            while (!check) {
                Console.WriteLine("Quelle age avez-vous?");
                bool conv = int.TryParse(Console.ReadLine(), out age);
                if (conv)
                {
                    if (age > 5 && age < 120)
                    {
                        if (age >= 60)
                        {
                            rabaisPlusDe60 = true;
                        }
                        check = true;
                    }
                    else
                    {
                        Console.WriteLine("Vous devez etre agee entre 5-120 ans");
                    }
                }
                else
                {
                    Console.WriteLine("Entre un int");
                }
            }
            //Type d'abonnement
            check = false;
            while (!check)
            {
                Console.WriteLine("Quelle est le type de votre abonnement? (1=Individuel | 2=Couple | 3= Familial)");
                type = Console.ReadLine();

                if (type == "1" | type == "2" | type == "3")
                {
                    check = true;
                }
                else
                {
                    Console.WriteLine("Entrez soit 1,2,3");
                }

            }
            //Duree de l'abonnement
            check = false;
            while (!check)
            {
                Console.WriteLine("Quelle est la duree de votre abonnement?");
                bool conv = int.TryParse(Console.ReadLine(), out dure);
                if (conv)
                {
                    if (dure == 3 || dure == 6 || dure == 9 || dure == 12)
                    {
                        check = true;
                        if (dure == 6)
                        {
                            switch (type)
                            {
                                case "1":
                                    rabaisActivite = 0.05f;
                                    break;
                                case "2":
                                    rabaisActivite = 0.07f;
                                    break;
                                case "3":
                                    rabaisActivite = 0.1f;
                                    break;

                            }
                        }else if (dure == 9)
                        {
                            switch (type)
                            {
                                case "1":
                                    rabaisActivite = 0.10f;
                                    break;
                                case "2":
                                    rabaisActivite = 0.12f;
                                    break;
                                case "3":
                                    rabaisActivite = 0.15f;
                                    break;

                            }
                        }
                        else//dure == 12
                        {
                            switch (type)
                            {
                                case "1":
                                    rabaisActivite = 0.15f;
                                    break;
                                case "2":
                                    rabaisActivite = 0.17f;
                                    break;
                                case "3":
                                    rabaisActivite = 0.2f;
                                    break;

                            }
                        }
                        

                    }
                    else
                    {
                        Console.WriteLine("Entrez soit 3,6,9 ou 12");
                    }
                }
                else
                {
                    Console.WriteLine("Entre un int");
                }
            }
            //activite cardio
            check = false;
            while (!check)
            {
                Console.WriteLine("Voulez-vous participez a l'activite de cardio? (O/N)?");
                string choix = Console.ReadLine();

                if (choix == "O")
                {
                    switch (type)
                    {
                        case "1":
                            coutActivite += (dure / 3) * 120;
                            break;
                        case "2":
                            coutActivite += (dure / 3) * 140;
                            break;
                        case "3":
                            coutActivite += (dure / 3) * 160;
                            break;
                    }
                    check = true;
                }else if (choix=="N")
                {

                    check = true;
                }
                else
                {
                    Console.WriteLine("Entrez soit 1,2,3");
                }
            }
            //activite musculation
            check = false;
            while (!check)
            {
                Console.WriteLine("Voulez-vous participez a l'activite de musculation? (O/N)?");
                string choix = Console.ReadLine();

                if (choix == "O")
                {
                    switch (type)
                    {
                        case "1":
                            coutActivite += (dure / 3) * 110;
                            break;
                        case "2":
                            coutActivite += (dure / 3) * 120;
                            break;
                        case "3":
                            coutActivite += (dure / 3) * 140;
                            break;
                    }
                    check = true;
                }
                else if (choix == "N")
                {

                    check = true;
                }
                else
                {
                    Console.WriteLine("Entrez soit 1,2,3");
                }
            }
            //activite kickboxing
            check = false;
            while (!check)
            {
                Console.WriteLine("Voulez-vous participez a l'activite de kickboxing? (O/N)?");
                string choix = Console.ReadLine();

                if (choix == "O")
                {
                    switch (type)
                    {
                        case "1":
                            coutActivite += (dure / 3) * 100;
                            break;
                        case "2":
                            coutActivite += (dure / 3) * 110;
                            break;
                        case "3":
                            coutActivite += (dure / 3) * 130;
                            break;
                    }
                    check = true;
                }
                else if (choix == "N")
                {

                    check = true;
                }
                else
                {
                    Console.WriteLine("Entrez soit 1,2,3");
                }
            }
            //activite zumba
            check = false;
            while (!check)
            {
                Console.WriteLine("Voulez-vous participez a l'activite de zumba? (O/N)?");
                string choix = Console.ReadLine();

                if (choix == "O")
                {
                    switch (type)
                    {
                        case "1":
                            coutActivite += (dure / 3) * 100;
                            break;
                        case "2":
                            coutActivite += (dure / 3) * 110;
                            break;
                        case "3":
                            coutActivite += (dure / 3) * 130;
                            break;
                    }
                    check = true;
                }
                else if (choix == "N")
                {

                    check = true;
                }
                else
                {
                    Console.WriteLine("Entrez soit 1,2,3");
                }
            }
            
            Client Janier = new Client("Janier",age,type,coutActivite,dure,rabaisActivite);
            string facture = Janier.genererFacture();
            Console.WriteLine(facture);
            //ecrire cout facture
        }
    }
}
